const sort = {};

sort.get = (type = '') => {
  switch (type.toLowerCase()) {
    case 'bydatetime':
      return (article1, article2) => article2.getDateTime() - article1.getDateTime();
    default:
      return article => article;
  }
};

module.exports = sort;
