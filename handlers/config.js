const fs = require('fs');

const json = fs.readFileSync('config.json', 'utf8');

module.exports = JSON.parse(json);
