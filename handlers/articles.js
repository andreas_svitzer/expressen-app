const Feedparser = require('feedparser-promised');
const Config = require('./config');
const Article = require('../models/article');
const Sorting = require('./sort');
const Filter = require('./filter');

const articles = {};

const handleError = reason => console.warn(`Warning: ${reason.message}`);

const createPromises = () => {
  const promises = [];
  Config.categories.forEach((category) => {
    promises.push(Feedparser.parse(Config.baseUrl + category).catch(handleError));
  });
  return promises;
};

const allArticles = categoryArticles => [].concat(...[].concat(...categoryArticles));

articles.get = (limit = Config.defaultLimit) => new Promise((resolve, reject) => {
  Promise.all(createPromises()).then((categoryArticles) => {
    resolve(allArticles(categoryArticles)
      .filter(Filter.get('byExists'))
      .map(article => Article(article))
      .filter(Filter.get('byGuid'))
      .sort(Sorting.get('byDateTime'))
      .slice(0, limit));
  }).catch((reason) => {
    reject(reason);
  });
});

module.exports = articles;
