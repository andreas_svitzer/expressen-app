const filter = {};

filter.get = (type = '') => {
  const seen = [];
  switch (type.toLowerCase()) {
    case 'byguid':
      return (article) => {
        if (seen[article.guid]) return null;
        seen[article.guid] = true;
        return article;
      };
    case 'byexists':
      return article => (article || null);
    default:
      return article => article;
  }
};

module.exports = filter;
