const Express = require('express');

const App = Express();
const port = 3001;
const MyRoutes = require('./routes');

App.use(MyRoutes);

App.listen(port, () => console.log(`Example app listening on port ${port}!`));

module.exports = App;
