const Article = (article) => {
  const newArticle = {};
  newArticle.guid = article.guid;
  newArticle.title = article.title;
  newArticle.link = article.link;
  newArticle.date = article.pubDate;
  newArticle.getDateTime = () => new Date(article.pubDate);
  return newArticle;
};

module.exports = Article;
