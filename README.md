# Setup
- Install npm packages with `npm install`
- Put a big smile on your face, knowing that you are done with the setup. 

# Run
- Start with `npm start app.js`
- Test it out on http://localhost:3001/
