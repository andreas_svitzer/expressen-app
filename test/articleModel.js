const Chai = require('chai');
const Article = require('../models/article');

describe('Article model', () => {
  it('should create new object', () => {
    Chai.expect(Article({
      test: 123,
      guid: 123,
      title: 'Test title',
      link: 'link.com',
      pubDate: '2018-11-02',
    })).to.contain.keys('guid', 'title', 'link', 'date', 'getDateTime');
  });
});
