const Feedparser = require('feedparser-promised');
const Sinon = require('sinon');
const SinonStubPromise = require('sinon-stub-promise');
const Chai = require('chai');
const Articles = require('../handlers/articles');

SinonStubPromise(Sinon);

describe('Articles handler', () => {
  let stubedFeedparser;
  const articles = [{
    guid: 'adc123',
    title: 'Title nyheter',
    link: 'link1.com',
    pubDate: '2018-09-02',
  }, {
    guid: '123adc',
    title: 'Title2 nyheter',
    link: 'link2.com',
    pubDate: '2018-11-02',
  }, {
    guid: 'klm123',
    title: 'Title gt',
    link: 'link3.com',
    pubDate: '2017-11-02',
  }, {
    guid: 'pol1911',
    title: 'Title 123',
    link: 'link4.com',
    pubDate: '2016-11-02',
  }];
  beforeEach(() => {
    stubedFeedparser = Sinon.stub(Feedparser, 'parse');
  });
  afterEach(() => {
    stubedFeedparser.restore();
  });
  it('should return 3 articles', () => {
    stubedFeedparser.withArgs('https://feeds.expressen.se/nyheter').returns(new Promise(resolve => resolve([articles[0], articles[1]])));
    stubedFeedparser.withArgs('https://feeds.expressen.se/gt').returns(new Promise(resolve => resolve([articles[2]])));
    stubedFeedparser.withArgs('https://feeds.expressen.se/sport').returns(Promise.reject(Error('Not a feed')));
    stubedFeedparser.returns(new Promise(resolve => resolve([articles[3]])));
    Articles.get(3)
      .then((result) => {
        Chai.expect(result.length).to.equal(3);
        Chai.expect(result[0].guid).to.equal(articles[1].guid);
        Chai.expect(result[1].guid).to.equal(articles[0].guid);
        Chai.expect(result[2].guid).to.equal(articles[2].guid);
      }).catch((reason) => {
        console.error(reason);
      });
  });
  it('should return empty result', () => {
    stubedFeedparser.returns(Promise.reject(Error('Not a feed')));
    Articles.get()
      .then((result) => {
        Chai.expect(result.length).to.equal(0);
      }).catch((reason) => {
        console.error(reason);
      });
  });
});
