const Request = require('supertest');
const Express = require('express');
const MyRoutes = require('../routes');
const Config = require('../handlers/config');

const App = Express();

App.use(MyRoutes);

describe('GET /latest?limit=10', () => {
  it('respond with collection of articles sorted by date', (done) => {
    const numberOfArticles = 10;
    Request(App)
      .get(`/latest?limit=${numberOfArticles}`)
      .expect((res) => {
        if (res.body.length !== numberOfArticles) {
          throw new Error(`Number of articles not equal to ${numberOfArticles}`);
        }
      })
      .expect((res) => {
        for (let i = 0; i < res.body.length; i += 1) {
          if (res.body[i + 1] && res.body[i].date < res.body[i + 1].date) {
            throw new Error('Collection is not sorted by latest date');
          }
        }
      })
      .expect(200, done);
  });
});

describe('GET /latest?limit=0', () => {
  it('respond with collection of articles sorted by date', (done) => {
    const numberOfArticles = 0;
    Request(App)
      .get(`/latest?limit=${numberOfArticles}`)
      .expect((res) => {
        if (res.body.length !== numberOfArticles) {
          throw new Error(`Number of articles not equal to ${numberOfArticles}`);
        }
      })
      .expect((res) => {
        for (let i = 0; i < res.body.length; i += 1) {
          if (res.body[i + 1] && res.body[i].date < res.body[i + 1].date) {
            throw new Error('Collection is not sorted by latest date');
          }
        }
      })
      .expect(200, done);
  });
});

describe('GET /latest', () => {
  it('respond 200', (done) => {
    const numberOfArticles = Config.defaultLimit;
    Request(App)
      .get('/latest')
      .expect((res) => {
        if (res.body.length !== numberOfArticles) {
          throw new Error(`Number of articles not equal to ${numberOfArticles}`);
        }
      })
      .expect(200, done);
  });
});

describe('GET /latest123', () => {
  it('respond 404', (done) => {
    Request(App)
      .get('/latest123')
      .expect(404, done);
  });
});
