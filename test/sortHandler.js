const Chai = require('chai');
const Sort = require('../handlers/sort');

describe('Sort handler', () => {
  it('should return function', () => {
    const byDate = Sort.get('byDate');
    Chai.expect(byDate).to.be.a('function');
  });

  it('should sort articles by date', () => {
    const result = [{
      getDateTime: () => new Date('2018-11-08T19:52:56.000Z'),
    }, {
      getDateTime: () => new Date('2018-11-03T19:52:56.000Z'),
    }, {
      getDateTime: () => new Date('2018-11-09T18:50:27.000Z'),
    }].sort(Sort.get('byDateTime'));
    result.forEach((article, index) => {
      if (result[index + 1]) {
        Chai.expect(article.getDateTime()).to.be.least(result[index + 1].getDateTime());
      }
    });
  });

  it('should sort articles by default expect same result back', () => {
    const obj1 = {
      getDateTime: () => new Date('2018-11-03T19:52:56.000Z'),
    };
    const obj2 = {
      getDateTime: () => new Date('2018-11-09T18:50:27.000Z'),
    };
    const arr = [obj1, obj2, obj1];
    const result = arr.sort(Sort.get());
    Chai.expect(result).to.deep.equal(arr);
  });
});
