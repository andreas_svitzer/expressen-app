const Chai = require('chai');
const Config = require('../handlers/config');

describe('Config handler', () => {
  it('should have baseUrl config', () => {
    Chai.expect(Config.baseUrl).to.be.an('string');
    Chai.expect(Config.baseUrl.length).to.be.at.least(1);
  });

  it('should have categories', () => {
    Chai.expect(Config.categories).to.be.an('array');
    Chai.expect(Config.categories.length).to.be.least(1);
  });

  it('should have defaultLimit config and be an number', () => {
    Chai.expect(Config.defaultLimit).to.be.an('number');
  });

  it('should have cache config and be an string', () => {
    Chai.expect(Config.cache).to.be.an('string');
  });
});
