const Chai = require('chai');
const Filter = require('../handlers/filter');

describe('Filter handler', () => {
  it('should return function', () => {
    const byDate = Filter.get('byGuid');
    Chai.expect(byDate).to.be.a('function');
  });

  it('should filter articles by guid expect one result back', () => {
    const obj = {
      guid: 'a123',
    };
    const result = [obj, obj, obj].filter(Filter.get('byGuid'));
    Chai.expect(result).to.contain(obj);
    Chai.expect(result.length).to.equal(1);
  });

  it('should filter articles by guid expect two results back', () => {
    const obj1 = {
      guid: 'a123',
    };
    const obj2 = {
      guid: 'a1234',
    };
    const result = [obj1, obj2, obj1].filter(Filter.get('byGuid'));
    Chai.expect(result).to.contain(obj1);
    Chai.expect(result).to.contain(obj2);
    Chai.expect(result.length).to.equal(2);
  });

  it('should filter articles by default expect all results back', () => {
    const obj1 = {
      guid: 'a123',
    };
    const obj2 = {
      guid: 'a1234',
    };
    const arr = [obj1, obj2, obj1];
    const result = arr.filter(Filter.get());
    Chai.expect(result).to.deep.equal(arr);
  });

  it('should filter articles by exists expect one result back', () => {
    const obj1 = {
      guid: 'a123',
    };
    const obj2 = undefined;
    const arr = [obj1, obj2];
    const result = arr.filter(Filter.get('byExists'));
    Chai.expect(result).to.contain(obj1);
    Chai.expect(result).to.not.contain(obj2);
    Chai.expect(result.length).to.equal(1);
  });
});
