
const Express = require('express');
const Apicache = require('apicache');

const Articles = require('../handlers/articles');
const Config = require('../handlers/config');

const Router = Express.Router();

/* *************************
 * Middlewares
 ************************** */
const cache = Apicache.middleware;

/* *************************
 * Routes
 ************************** */

Router.get('/', (req, res) => {
  res.send('Hello World!');
});

// /latest?limit=5 (see default in config)
Router.get('/latest', cache(Config.cache), (req, res) => {
  Articles.get(req.query.limit)
    .then(values => res.send(values))
    .catch((reason) => {
      console.error(reason);
      res.status(500)
        .send({ error: reason.message });
    });
});

module.exports = Router;
