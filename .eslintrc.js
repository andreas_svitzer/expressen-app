module.exports = {
    "extends": "airbnb-base",
    "plugins": [
        "chai-friendly"
    ],
    "env":{
        "jest":true
    },
    "rules": {
        "no-shadow": "off"
    }
};